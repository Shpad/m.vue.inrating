import Vuex from 'vuex'
import axios from 'axios';


window.storeFeeds=new Vuex.Store({
  state: {
    allfeeds:[],
    nextPage: 2,
    metaPage: 0,
  },
  actions: {
    LOAD_USER_FEEDS({commit, state}){
      state.allfeeds=[];

      axios.get('v1/me/feeds/new/all')
        .then( res=>{
            commit('USER_FEED', {allfeeds: res.data.data});
        })
    },
    LOAD_NEXT_PAGE_USER_FEEDS({commit, state},info) {
      if(Number(state.metaPage) < Number(state.nextPage)){
        state.load = false;
        return
      }

      axios.post('v1/contests/users/all?page='+state.nextPage, )
        .then(res =>{
          let url = res.data.meta.current_page;
          state.allfeeds = state.allfeeds.concat(res.data.data);
          state.nextPage = url + 1;
          res.data.data.length < 16 ? state.load = false : '';
        })
    }
  },
  mutations: {
    USER_FEED(state, {allfeeds}){
      state.allfeeds = allfeeds;
    },

  },
  getters: {
    getUserFeeds(state){
      return state.allfeeds;
    }
  }
});
