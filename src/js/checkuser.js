import Vue from 'vue'
import event from 'JsLib/emit.js'


Vue.prototype.$userStatus = (_user) =>{

    switch (_user.status) {
      case 'banned': {
        event.$emit('bannedUser');
        return false;
        break;
      }
      case 'banned_avatar': {
        event.$emit('bannedUserAvatar');
        return false;
        break;
      }
      case 'active': {
        return true;
        break;
      }
      case 'admin': {
        return true;
        break;
      }
      case 'guest': {
        event.$emit('guestUser');
        return false;
        break;
      }
      case 'no_chat_user': {
        event.$emit('noChatUser');
        return false;
        break;
      }


    }
};
