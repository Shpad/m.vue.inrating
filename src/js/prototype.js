import Vue from 'vue'


Vue.prototype.$unixTime = (_time = 0, _type = true, _length, monthIndex) => {
  let u = new Date();
  let ru = ['Сегодня', 'Вчера'];
  let en = ['Today', 'Yesterday'];
  let uk = ['Сьогодні', 'Вчора'];
  let month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  let activeDate = [];
  if (_type) {
    u = new Date(_time * 1000);
  } else {
    //@TODO on IPhone don't work
    u = new Date(_time);
  }

  switch (localStorage.getItem('lang')) {
    case 'ru':
      activeDate = ru;
      break;
    case 'en':
      activeDate = en;
      break;
    case 'uk':
      activeDate = uk;
      break;
  }

  if (_length !== 'full' && _length !== 'contest') {
    let checkDate = new Date();
    let checkDay = ('0' + checkDate.getDate()).slice(-2);
    let checkMonth = ('0' + Number(checkDate.getMonth() + 1)).slice(-2);
    let checkYear = checkDate.getFullYear();
    let checkDateString = checkDay + '.' + checkMonth + '.' + checkYear;

    let dateCheckPost = ('0' + u.getDate()).slice(-2) +
      '.' + ('0' + Number(u.getMonth() + 1)).slice(-2) +
      '.' + u.getFullYear();


    if (checkDateString === dateCheckPost) {
      return activeDate[0] + ' / ' + ('0' + u.getHours()).slice(-2) +
        ':' + ('0' + u.getMinutes()).slice(-2);
    }
    else if (checkDate.getDate() - 1 === u.getDate()) {
      return activeDate[1] + ' / ' + ('0' + u.getHours()).slice(-2) +
        ':' + ('0' + u.getMinutes()).slice(-2);
    }
    else {
      return ('0' + u.getDate()).slice(-2) +
        '.' + ('0' + Number(u.getMonth() + 1)).slice(-2) +
        '.' + u.getFullYear() +
        ' / ' + ('0' + u.getHours()).slice(-2) +
        ':' + ('0' + u.getMinutes()).slice(-2)
    }
  }
  else if (_length === 'contest') {
    if (monthIndex === 'day') {
      return ('0' + u.getDate()).slice(-2) +
        '.' + ('0' + Number(u.getMonth() + 1)).slice(-2) +
        '.' + u.getFullYear();
    }
    else {
      return (month[u.getMonth()] +
        ' ' + u.getFullYear());
    }
  }
  else {
    return ('0' + u.getDate()).slice(-2) +
      '.' + ('0' + Number(u.getMonth() + 1)).slice(-2) +
      '.' + u.getFullYear() +
      ' / ' + ('0' + u.getHours()).slice(-2) +
      ':' + ('0' + u.getMinutes()).slice(-2);
  }


};
