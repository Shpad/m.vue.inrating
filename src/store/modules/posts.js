import {ERROR, REQUEST, SUCCESS} from "Store/types";
import api from "utils/api";

/**
 * Types
 * */
export const moduleName = 'posts/';

/** Actions types*/
export const FETCH_POST = 'FETCH_POST';

/** Mutation types*/
// Actions types + REQUEST or SUCCESS or ERROR
export const CHANGE_SHOW_LOADING = 'CHANGE_SHOW_LOADING';
export const FETCH = 'FETCH';
export const ADD_POST = 'ADD_POST';

/** Getters types*/
export const LOADING = 'LOADING';
export const POST = 'POST';

const module = {
  namespaced: true,
  state: {
    posts: {},
    postSlugs: [],
    maxCount: 500,
    loading: false,
    showLoading: true,
    error: null
  },
  mutations: {
    [FETCH_POST + REQUEST](state) {
      state.loading = true;
      state.error = null;
    },
    [FETCH_POST + SUCCESS](state, payload) {
      state.loading = false;
      state.posts = {
        ...payload.postsTemp,
        [payload.postSlug]: payload.res
      };
      state.postSlugs = [...payload.postSlugsOrder, payload.postSlug];
    },
    [FETCH_POST + ERROR](state, error) {
      state.loading = false;
      state.error = error;
    },
    [CHANGE_SHOW_LOADING](state, payload) {
      state.showLoading = payload;
    },
    [FETCH + SUCCESS](state) {
      state.loading = false;
      state.showLoading = true;
    },
    [ADD_POST](state, payload) {
      const {data, postSlug} = payload;

      if(state.posts[postId]) {
        state.posts = {
          ...state.posts,
          [postId]: [...data, ...state.posts[postSlug]]
        };
      }
    }
  },
  actions: {
    async [FETCH_POST]({commit, state}, payload) {
      if(state.posts[payload]) {
        commit(CHANGE_SHOW_LOADING, false);
      }

      commit(FETCH_POST + REQUEST);

      const data = new FormData();
      data.append('slug', payload);

      try {
        let res = await api.users.getPost(data);

        let postSlugsOrder = state.postSlugs.filter(slug => slug !== payload);
        let postsTemp = {...state.posts};

        if(postSlugsOrder.length >= state.maxCount) {
          delete postsTemp[postSlugsOrder[0]];
          postSlugsOrder = postSlugsOrder.slice(1);
        }

        let result = {
          postSlug: payload,
          res: res.data,
          postSlugsOrder,
          postsTemp
        };

        if(state.showLoading === false) commit(CHANGE_SHOW_LOADING, true);
        commit(FETCH_POST + SUCCESS, result);

      } catch (e) {
        if(state.showLoading === false) commit(CHANGE_SHOW_LOADING, true);
        commit(FETCH_POST + ERROR, e.response);
      }
    },
    // async [ADD_POST]({commit}, payload) {
    //   commit(FETCH_POST + REQUEST);
    //
    //   try {
    //     const data = new FormData();
    //     data.append('id', payload);
    //     let res = await api.albums.getAlbum(data);
    //
    //     let result = {
    //       postId: payload,
    //       data: res.data
    //     };
    //
    //     commit(FETCH_POST + SUCCESS, result);
    //   } catch (e) {
    //     commit(FETCH_POST + ERROR, e.response);
    //   }
    // }
  },
  getters: {
    [LOADING](state) {
      return state.loading && state.showLoading
    },
    [POST]: state => slug => {
      return state.posts[slug]
    }
  }
};

export default module;
