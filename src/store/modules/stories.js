import api from 'utils/api';

/**
 * Types
 * */
export const moduleName = 'stories/';

/** Actions types*/
export const DELETE_STORY = 'DELETE_STORY';


/** Mutation types*/

/** Getters types*/

const module = {
  namespaced: true,
  state: {

  },
  mutations: {
    /** fetch current user */

  },
  actions: {
    async [DELETE_STORY]({}, payload) {

      try {
        let res = await api.stories.deleteStory(payload);
        console.log(res);

      } catch (e) {
        console.log(e);
      }
    },

  },
  getters: {

  }
};

export default module;
