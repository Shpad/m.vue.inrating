import api from "utils/api";
import {ERROR, REQUEST, SUCCESS} from "Store/types";

/**
 * Types
 * */
export const moduleName = 'mainPosts/';

/** Actions types*/
export const FETCH_MAIN_POSTS = 'FETCH_MAIN_POSTS';
export const FETCH_MAIN_POSTS_NEXT_PAGE = 'FETCH_MAIN_POSTS_NEXT_PAGE';
export const FETCH_NEW_POSTS = 'FETCH_NEW_POST';

/** Mutation types*/
// Actions types + REQUEST or SUCCESS or ERROR
export const CHANGE_SHOW_LOADING = 'CHANGE_SHOW_LOADING';
export const FETCH = 'FETCH';
export const CHANGE_IS_CHANGED_FILTERS = 'CHANGE_IS_CHANGED_FILTERS';
export const RESET_POSTS = 'RESET_POSTS';
export const CHANGE_IS_FETCH_NEW_POSTS = 'CHANGE_IS_NEW_POSTS';
export const RESET_MAIN_PAGE = 'RESET_MAIN_PAGE';
export const DELETE_POST_FROM_MAIN_PAGE = 'DELETE_POST_FROM_MAIN_PAGE';

/** Getters types*/
export const LOADING = 'LOADING';
export const MAIN_POSTS = 'MAIN_POSTS';

const module = {
  namespaced: true,
  state: {
    mainPosts: [],
    lastPage: 1,
    nextPage: 2,
    timestamp: null,
    loading: false,
    showLoading: true,
    error: null,
    isChangedFilters: false,
    fetchNew: true
  },
  mutations: {
    [FETCH_MAIN_POSTS + REQUEST](state) {
      state.loading = true;
      state.error = null;
    },
    [FETCH_MAIN_POSTS + SUCCESS](state, payload) {
      state.loading = false;
      state.lastPage = payload.res.meta.last_page;
      state.nextPage = payload.res.meta.current_page + 1;
      state.mainPosts = payload.res.data;
      state.timestamp = payload.timestamp;
      state.fetchNew = false;
    },
    [FETCH_MAIN_POSTS + ERROR](state, error) {
      state.loading = false;
      state.error = error;
    },
    [FETCH_MAIN_POSTS_NEXT_PAGE + REQUEST](state) {
      state.loading = true;
      state.error = null;
    },
    [FETCH_MAIN_POSTS_NEXT_PAGE + SUCCESS](state, payload) {
      state.loading = false;
      state.nextPage = payload.meta.current_page + 1;
      state.mainPosts.push(...payload.data);
    },
    [FETCH_MAIN_POSTS_NEXT_PAGE + ERROR](state, error) {
      state.loading = false;
      state.error = error;
    },
    [CHANGE_SHOW_LOADING](state, payload) {
      state.showLoading = payload;
    },
    [FETCH + SUCCESS](state) {
      state.loading = false;
      state.showLoading = true;
      state.fetchNew = false;
    },
    [CHANGE_IS_CHANGED_FILTERS](state, payload) {
      state.isChangedFilters = payload;
      state.fetchNew = true;
    },
    [RESET_POSTS](state) {
      state.mainPosts = [];
    },
    [CHANGE_IS_FETCH_NEW_POSTS](state, payload) {
      state.fetchNew = payload;
    },
    [RESET_MAIN_PAGE](state) {
      state.mainPosts = [];
      state.fetchNew = true;
    },
    [DELETE_POST_FROM_MAIN_PAGE](state, payload) {
      state.mainPosts = state.mainPosts.filter(item => item.id !== payload);
    }
  },
  actions: {
    async [FETCH_MAIN_POSTS]({commit, state, rootState}) {
      if(!state.fetchNew || state.loading || !rootState.user.user.nickname){
        return;
      }

      if(state.mainPosts.length > 0 && !state.isChangedFilters) {
        commit(CHANGE_SHOW_LOADING, false);
      }
      if(state.isChangedFilters) {
        commit(RESET_POSTS);
      }
      commit(FETCH_MAIN_POSTS + REQUEST);

      const timestamp = Math.ceil(Date.now() / 1000) + 5;
      const data = new FormData();
      data.append('timestamp', timestamp);
      data.append('limit', '16');

      try {
        let res = await api.users.getPosts(data);

        if(state.mainPosts.length > 0 && res.data.data[0].id === state.mainPosts[0].id && !state.isChangedFilters) {
          commit(FETCH + SUCCESS);
          return;
        }

        let result = {
          res: res.data,
          timestamp
        };

        commit(FETCH_MAIN_POSTS + SUCCESS, result);
        if(state.showLoading === false) commit(CHANGE_SHOW_LOADING, true);
        if(state.isChangedFilters) commit(CHANGE_IS_CHANGED_FILTERS, false)
      } catch (e) {
        commit(FETCH_MAIN_POSTS + ERROR, e.response.data);
        if(state.showLoading === false) commit(CHANGE_SHOW_LOADING, true);
      }
    },
    async [FETCH_MAIN_POSTS_NEXT_PAGE]({commit, state}, payload) {
      if(state.lastPage < state.nextPage || state.loading) {
        return;
      }

      const {onSuccess} = payload;

      commit(FETCH_MAIN_POSTS_NEXT_PAGE + REQUEST);

      const data = new FormData();
      data.append('timestamp', state.timestamp);
      data.append('page', state.nextPage);
      data.append('limit', '16');

      try {
        let res = await api.users.getPosts(data);

        commit(FETCH_MAIN_POSTS_NEXT_PAGE + SUCCESS, res.data);
        onSuccess();
      } catch (e) {

        commit(FETCH_MAIN_POSTS_NEXT_PAGE + ERROR, e.response);
      }
    },
    [FETCH_NEW_POSTS]({commit, dispatch}){
      commit(CHANGE_IS_FETCH_NEW_POSTS, true);
      dispatch(FETCH_MAIN_POSTS);
    }
  },
  getters: {
    [LOADING](state) {
      return state.loading && state.showLoading
    },
    [MAIN_POSTS](state) {
      return state.mainPosts
    }
  }
};

export default module;
