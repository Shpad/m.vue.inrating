import api from 'utils/api';
import {ERROR, REQUEST, SUCCESS} from "Store/types";
import {CHANGE_SUBSCRIBE_USER_GIFTS, moduleName as giftsModule} from "Store/modules/authUser/gifts";
import {CHANGE_SUBSCRIBE_USER_FEEDS, moduleName as feedsModule} from "Store/modules/feeds";
import {CHANGE_SUBSCRIBE_USER_COUNT, moduleName as userModule} from "Store/modules/authUser/user";

/**
 * Types
 * */
export const moduleName = 'users/';

/** Actions types*/
export const CHANGE_SUBSCRIBE_USER = 'CHANGE_SUBSCRIBE_USER';
export const FETCH_USER = 'FETCH_USER';
export const VOTE_USER = 'VOTE_USER';
export const SEND_CHIN_CHIN = 'SEND_CHIN_CHIN';

/** Mutation types*/
// Actions types + REQUEST or SUCCESS or ERROR
export const CHANGE_SHOW_LOADING = 'CHANGE_SHOW_LOADING';
export const FETCH = 'FETCH';
export const CHANGE_USER_FAMILY_STATUS = 'CHANGE_USER_FAMILY_STATUS';
export const ADD_POST_TO_USER = 'ADD_POST_TO_USER';
export const DELETE_POST_FROM_USER = 'DELETE_POST_FROM_USER';

/** Getters types*/
export const LOADING = 'LOADING';
export const USER = 'USER';
export const USER_ERROR = 'USER_ERROR';

const module = {
  namespaced: true,
  state: {
    users: {},
    userNicknames: [],
    maxCount: 200,
    loading: false,
    showLoading: true,
    error: null
  },
  mutations: {
    [CHANGE_SUBSCRIBE_USER + REQUEST](state) {

    },
    [CHANGE_SUBSCRIBE_USER + SUCCESS](state, payload) {
      if(state.users[payload.nickname]) state.users[payload.nickname].is_subscribed = payload.state;
      state.users[payload.authUserNickname].subscriptions_count += (payload.state ? 1 : -1);
    },
    [CHANGE_SUBSCRIBE_USER + ERROR](state, payload) {

    },
    [FETCH_USER + REQUEST](state) {
      state.loading = true;
      state.error = null;
    },
    [FETCH_USER + SUCCESS](state, payload) {
      state.loading = false;
      state.users = {
        ...payload.usersTemp,
        [payload.nickname]: payload.res
      };
      state.userNicknames = [...payload.userNicknamesOrder, payload.nickname];
    },
    [FETCH_USER + ERROR](state, error) {
      state.loading = false;
      state.error = error;
    },
    [CHANGE_SHOW_LOADING](state, payload) {
      state.showLoading = payload;
    },
    [FETCH + SUCCESS](state) {
      state.loading = false;
      state.showLoading = true;
    },
    [CHANGE_USER_FAMILY_STATUS](state, payload) {
      if(state.users[payload.nickname]) state.users[payload.nickname].family_status = payload.status;
    },
    [VOTE_USER + REQUEST](state) {

    },
    [VOTE_USER + SUCCESS](state, payload) {
      if(payload.state) {
        state.users[payload.nickname].is_voted = true;
        state.users[payload.nickname].rating.level = payload.rating.level;
        state.users[payload.nickname].rating.proportion = payload.rating.proportion;
        state.users[payload.nickname].rating.value = payload.rating.value;
      }
    },
    [VOTE_USER + ERROR](state, error) {

    },
    [SEND_CHIN_CHIN + REQUEST](state) {

    },
    [SEND_CHIN_CHIN + SUCCESS](state, payload) {
      if(payload.state) state.users[payload.nickname].is_chined = true;
    },
    [SEND_CHIN_CHIN + ERROR](state, error) {

    },
    [ADD_POST_TO_USER](state, payload) {
      state.users[payload].posts_count = state.users[payload].posts_count + 1;
    },
    [DELETE_POST_FROM_USER](state, payload) {
      state.users[payload].posts_count = state.users[payload].posts_count - 1;
    }
  },
  actions: {
    async [CHANGE_SUBSCRIBE_USER]({commit, rootState}, payload) {
      commit(CHANGE_SUBSCRIBE_USER + REQUEST);

      const data = new FormData();
      data.append('nickname', payload.nickname);
      if (payload.delete === '1') {
        data.append('delete', payload.delete);
      }

      try {
        let res = await api.users.subscribeUser(data);

        let result = {
          state: res.data.state,
          nickname: payload.nickname,
          authUserNickname: rootState.user.user.nickname
        };

        commit(CHANGE_SUBSCRIBE_USER + SUCCESS, result);
        commit(giftsModule + CHANGE_SUBSCRIBE_USER_GIFTS, result, { root: true });
        commit(feedsModule + CHANGE_SUBSCRIBE_USER_FEEDS, result, { root: true });
        commit(userModule + CHANGE_SUBSCRIBE_USER_COUNT, res.data.state ? 1 : -1, { root: true });
      } catch (e) {
        commit(CHANGE_SUBSCRIBE_USER + ERROR, e.response.data);
      }
    },
    async [FETCH_USER]({commit, state, rootState}, payload) {
      if(state.users[payload]) {
        commit(CHANGE_SHOW_LOADING, false);
      }

      commit(FETCH_USER + REQUEST);

      try {
        let res;

        if(rootState.user.user.nickname === payload) {
          let temp = {...rootState.user.user};
          res = { data: temp };
        } else {
          const data = new FormData();
          data.append('nickname', payload);

          res = await api.users.getUser(data);
        }

        let userNicknamesOrder = state.userNicknames.filter(slug => slug !== payload);
        let usersTemp = {...state.users};

        if(userNicknamesOrder.length >= state.maxCount) {
          delete usersTemp[userNicknamesOrder[0]];
          userNicknamesOrder = userNicknamesOrder.slice(1);
        }

        let result = {
          nickname: payload,
          res: res.data,
          userNicknamesOrder,
          usersTemp
        };

        if(state.showLoading === false) commit(CHANGE_SHOW_LOADING, true);
        commit(FETCH_USER + SUCCESS, result);

      } catch (e) {
        if(state.showLoading === false) commit(CHANGE_SHOW_LOADING, true);
        commit(FETCH_USER + ERROR, e.response);
      }
    },
    async [VOTE_USER]({commit}, payload) {
      commit(VOTE_USER + REQUEST);

      const {nickname, onSuccess, onFail} = payload;

      const data = new FormData();
      data.append('nickname', nickname);

      try {
        let res = await api.users.voteUser(data);

        commit(VOTE_USER + SUCCESS, res.data);
        onSuccess(res.data);
      } catch (e) {
        commit(VOTE_USER + ERROR, e.response);
        onFail(e);
      }
    },
    async [SEND_CHIN_CHIN]({commit}, payload) {
      commit(SEND_CHIN_CHIN + REQUEST);

      const { nickname, onSuccess } = payload;

      const data = new FormData();
      data.append('nickname', nickname);

      try {
        let res = await api.user.sendChinChin(data);

        let result = {
          state: res.data.state,
          nickname
        };

        commit(SEND_CHIN_CHIN + SUCCESS, result);
        onSuccess(res.data)
      } catch (e) {
        commit(SEND_CHIN_CHIN + ERROR, e.response);
      }
    }
  },
  getters: {
    [LOADING](state) {
      return state.loading && state.showLoading
    },
    [USER]: state => nickname => {
      return state.users[nickname]
    },
    [USER_ERROR](state) {
      return state.error
    }
  }
};

export default module;
