import Vue from 'vue';
import Router from 'vue-router';
import LoginForm from 'ComponentsVue/LoginPage.vue'
import Registration from 'ComponentsVue/Registration.vue'
import ContinueRegister from 'ComponentsVue/ContinueRegister'
import Main from 'ComponentsVue/Main.vue';
import UserProfile from 'ComponentsVue/UserProfile.vue'
import ProfilePost from 'ComponentsVue/profile/content/ProfilePost.vue'
import ProfileSubscribers from 'ComponentsVue/profile/content/ProfileSubscribers.vue'
import ProfileSubscribition from 'ComponentsVue/profile/content/ProfileSubscribition.vue'
import ProfileBookmarks from 'ComponentsVue/profile/content/ProfileBookmarks.vue'
import ProfileMention from 'ComponentsVue/profile/content/ProfileMentions.vue'
import ProfileGift from 'ComponentsVue/profile/content/ProfileGifts.vue'
import ProfileScale from 'ComponentsVue/profile/content/ProfileScale.vue'
import PostAddCrop from 'ComponentsVue/postadd/PostAddCrop.vue'
import Feeds from 'ComponentsVue/Feed.vue'
import PostShow from 'ComponentsVue/PostShow.vue'
import PostStatistics from 'ComponentsVue/PostStatistics.vue'
import Search from 'ComponentsVue/Search.vue'
import searchAll from 'ComponentsVue/search/searchAll'
import searchPeople from 'ComponentsVue/search/searchPeople.vue'
import searchIntag from 'ComponentsVue/search/searchIntag.vue'
import searchGroup from 'ComponentsVue/search/searchGroup.vue'
import searchLocation from 'ComponentsVue/search/searchLocation.vue'
import Contests from 'ComponentsVue/Contests.vue'
import singleContest from 'ComponentsVue/contest/singleContest.vue'
import ChangeProfile from 'ComponentsVue/ChangeProfile.vue'
import Settings from 'ComponentsVue/Settings.vue'
import settingsMainPage from 'ComponentsVue/settings/settingsMainPage.vue'
import settingsRaitingPage from 'ComponentsVue/settings/settingsRaitingPage.vue'
import settingsCommonPage from 'ComponentsVue/settings/settingsCommonPage.vue'
import settingsStories from 'ComponentsVue/settings/settingsStories.vue'
import Chats from 'ComponentsVue/Chats.vue'
import chatTreads from 'ComponentsVue/chats/chatTreads.vue'
import chatSoloTread from 'ComponentsVue/chats/chatSoloTread.vue'
import chatNewsLetter from 'ComponentsVue/chats/chatNewsLetter.vue'
import newSoloTread from 'ComponentsVue/chats/newSoloTread.vue'
import groupChatCreate from 'ComponentsVue/chats/groupChatCreate.vue'
import Groups from 'ComponentsVue/Groups.vue'
import groupSolo from 'ComponentsVue/groups/groupSolo.vue'
import groupCreate from 'ComponentsVue/groups/groupCreate.vue'
import groupPost from 'ComponentsVue/groups/groupComponents/groupPost.vue'
import groupAlbums from 'ComponentsVue/groups/groupComponents/groupAlbums.vue'
import GroupVideos from 'ComponentsVue/groups/groupComponents/GroupVideos'
import GroupPostShow from 'ComponentsVue/groups/groupComponents/GroupPostShow'
import groupRequests from 'ComponentsVue/groups/groupComponents/groupRequests.vue'
import groupEdit from 'ComponentsVue/groups/groupComponents/groupEdit.vue'
import Album from 'ComponentsVue/groups/groupComponents/albums/albumSolo.vue'
import createAlbum from 'ComponentsVue/groups/groupComponents/albums/albumCreate.vue'
import groupDiscussions from 'ComponentsVue/groups/groupComponents/groupDiscussions.vue'
import groupUsers from 'ComponentsVue/groups/groupComponents/groupUsers.vue'
import groupInvite from 'ComponentsVue/groups/groupComponents/groupInvite'
import Discussion from 'ComponentsVue/groups/groupComponents/discussions/discussionSolo.vue'
import createDiscussion from 'ComponentsVue/groups/groupComponents/discussions/discussionCreate'
import Wallet from 'ComponentsVue/Wallet.vue'
import Stories from 'ComponentsVue/Stories'
import storyAdd from 'ComponentsVue/story/storyAdd'
import UserMonth from 'ComponentsVue/UsersMonth'
import Test from 'ComponentsVue/test'
import EditPost from 'ComponentsVue/EditPost'
import PublicOffer from 'ComponentsVue/PublicOffer'
import NetworkRules from 'ComponentsVue/NetworkRules'
import MonthPresents from 'ComponentsVue/MonthPresents'
import BuySuccess from 'ComponentsVue/BuySuccess'
import BuyError from 'ComponentsVue/BuyError'
import contestPresentsPage from 'ComponentsVue/other/NewContest'
import contestPresentsWeek from 'ComponentsVue/other/NewContestWeek'
import contestPresentsDay from 'ComponentsVue/other/NewContestDay'
import SoloIntag from 'ComponentsVue/SoloIntag'
import ResetPassword from 'ComponentsVue/modal/resetPassword';
import AddToBlackList from 'ComponentsVue/blacklist/AddToBlackList';
import ActiveBlackList from 'ComponentsVue/blacklist/ActiveBlackList';
import userPrivacy from 'ComponentsVue/settings/userPrivacy'
import ContestsRules from 'ComponentsVue/ContestsRules'
import ContestCreate from 'ComponentsVue/contest/contestCreate'
import Rating from 'ComponentsVue/Raiting.vue';

// const Rating = () => import('ComponentsVue/Raiting.vue');
// const Rating = resolve => {
//   require.ensure(['ComponentsVue/Raiting.vue'], () => {
//     resolve(require('ComponentsVue/Raiting.vue'))
//   })
// };

Vue.use(Router);

const routes = [
  {path: '/contest-rules', component: ContestsRules},
  {path: '/contest-rules-ios', component: ContestsRules},
  {path: '/rule-miss-ukraine', component: contestPresentsPage},
  {path: '/rule-miss-ukraine-week', component: contestPresentsWeek},
  {path: '/login', component: LoginForm},
  {path: '/reset-password', component: ResetPassword},
  {path: '/rule-miss-ukraine-day', component: contestPresentsDay},
  {path: '/public-offer-and-confidentiality', component: PublicOffer},
  {path: '/rule-page', component: NetworkRules},
  {path: '/prizes', component: MonthPresents},
  {path: '/buy/success', component: BuySuccess},
  {path: '/buy/error', component: BuyError},
  {path: '/registration', component: Registration},
  {path: '/continue-register', component: ContinueRegister},
  {path: '', component: Main},
  {path: '/rating', component: Rating},
  {path: '/groups', component: Groups},
  {path: '/buy', component: Wallet},
  {path: '/rating/top', component: UserMonth},
  {path: '/statictics/:statistics_id', component: PostStatistics},
  {path: '/messages/:thread_id', component: chatSoloTread},
  {path: '/new-chat/:user_chat', component: newSoloTread},
  {path: '/create-newsletter', component: chatNewsLetter},
  {path: '/create-groupchat', component: groupChatCreate},
  {path: '/story/:story_nickname', component: Stories},
  {path: '/add-story', component: storyAdd},
  {path: '/group', component: groupChatCreate},
  {path: '/group/:group_slug', component: groupSolo,
    children: [
      {
        path: '',
        component: groupPost,
      },
      {
        path: 'albums',
        component: groupAlbums,
      },
      {
        path: 'videos',
        component: GroupVideos,
      },
      {
        path: 'discussions',
        component: groupDiscussions,
      },
      {
        path: 'user',
        component: groupUsers,
      },
      {
        path: 'requests',
        component: groupRequests,
      },
      {
        path: 'edit',
        component: groupEdit,
      },
      {
        path: 'invite',
        component: groupInvite,
      }
    ]
  },
  {path: '/group/:group_slug/albums/:album_slug', component: Album},
  {path: '/group/:group_slug/discussion/:discussion_slug', component: Discussion},
  {path: '/group/:group_slug/create-album', component: createAlbum},
  {path: '/group/:group_slug/create-discussion', component: createDiscussion},
  {path: '/group-create', component: groupCreate},
  {path: '/edit-post/:post_slug', component: EditPost},
  {
    path: '/messages', component: Chats,
    children: [
      {
        path: '',
        component: chatTreads,
      },
    ]
  },
  {path: '/feeds', component: Feeds},
  {
    path: '/search', component: Search,
    children: [
      {
        path: '',
        component: searchAll
      },
      {
        path: 'user',
        component: searchPeople
      },
      {
        path: 'intag',
        component: searchIntag
      },
      {
        path: 'group',
        component: searchGroup
      },
      {
        path: 'location',
        component: searchLocation
      },
    ]
  },
  {
    path: '/setting', component: Settings,
    children: [
      {
        path: '',
        component: settingsCommonPage,
      },
      {
        path: 'rating',
        component: settingsRaitingPage,
      },
      {
        path: 'common',
        component: settingsMainPage,
      },
      {
        path: 'stories',
        component: settingsStories,
      }
    ]
  },
  {path: '/setting/user', component: ChangeProfile},
  {path: '/contests', component: Contests},
  {path: '/contest/:id', component: singleContest},
  {path: '/test', component: Test},
  {path: '/new-post/:group_slug/albums/:album_slug', component: PostAddCrop},
  {path: '/new-post/:group_slug', component: PostAddCrop},
  {path: '/new-post', component: PostAddCrop},
  {path: '/p/:slug', component: PostShow},
  {path: '/p/group/:slug', component: GroupPostShow},
  {path: '/intag/:intag', component: SoloIntag},
  {
    path: '/u/:nickname', component: UserProfile,
    children: [
      {
        path: '',
        meta: {type: 'post'},
        component: ProfilePost
      },
      {
        path: 'subscribers',
        meta: {type: 'subscribers'},
        component: ProfileSubscribers
      },
      {
        path: 'subscribition',
        meta: {type: 'subscribition'},
        component: ProfileSubscribition
      },
      {
        path: 'bookmark',
        meta: {type: 'bookmark'},
        component: ProfileBookmarks
      },
      {
        path: 'mention',
        meta: {type: 'mention'},
        component: ProfileMention
      },
      {
        path: 'gift',
        meta: {type: 'gift'},
        component: ProfileGift
      },
      {
        path: 'statistics',
        meta: {type: 'scale'},
        component: ProfileScale
      },
    ]
  },
  {path: '/add-to-black-list', component: AddToBlackList},
  {path: '/active-black-list', component: ActiveBlackList},
  {path: '/setting/private-settings', component: userPrivacy},
  {path: '/contest-create', component: ContestCreate},

]

export default new Router({
  mode: 'history',
  routes: routes
})

