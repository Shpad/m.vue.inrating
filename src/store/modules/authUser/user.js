import api from 'utils/api';
import {ERROR, REQUEST, SUCCESS} from "Store/types";
import {CHANGE_USER_FAMILY_STATUS, moduleName as usersModule} from "Store/modules/users/users";
import {ACCEPT_USER_GIFT, moduleName as giftsModule} from "Store/modules/authUser/gifts";

/**
 * Types
 * */
export const moduleName = 'user/';

/** Actions types*/
export const FETCH_CURRENT_USER = 'FETCH_CURRENT_USER';
export const ADD_LOADED = 'ADD_LOADED';
export const CHANGE_FAMILY_STATUS = 'CHANGE_FAMILY_STATUS';
export const ACCEPT_GIFT = 'ACCEPT_GIFT';

/** Mutation types*/
// Actions types + REQUEST or SUCCESS or ERROR
export const ADD_POST_TO_AUTH_USER = 'ADD_POST_TO_AUTH_USER';
export const DELETE_POST_FROM_AUTH_USER = 'DELETE_POST_FROM_AUTH_USER';
export const CHANGE_SUBSCRIBE_USER_COUNT = 'CHANGE_SUBSCRIBE_USER_COUNT';

/** Getters types*/
export const AUTH_USER = 'AUTH_USER';
export const LOADED = 'LOADED';

const module = {
  namespaced: true,
  state: {
    user: {
      active_gift: {
        id: null,
        image: '',
        name: '',
      },
      activity_statuses: [],
      avatar_image: {
        id: null,
        mentioned_users_count: null,
        url: '',
        url_large: '',
        url_medium: '',
        url_small: '',
      },
      birth_date: null,
      birth_date_timestamp: null,
      bookmarks_count: null,
      comments: null,
      contest_entry: null,
      email: '',
      family_status: '',
      gender: '',
      geo_id: null,
      id: null,
      is_active_story: null,
      is_chined: null,
      is_online: null,
      is_story_seen: null,
      is_subscribed: null,
      is_voted: null,
      last_online_timestamp: null,
      lastname: null,
      name: null,
      nickname: null,
      off_page: null,
      personal_status: null,
      phone: null,
      posts_count: null,
      privacy_settings: {
        chat: null,
        profile: null,
        subscribe: null,
        subscribers: null,
        subscriptions: null
      },
      rating: {
        level: null,
        proportion: null,
        value: null,
      },
      settings: {
        chat_langs: {
          main_lang: null,
          sub_langs: []
        },
        lang: null,
        sounds: null,
      },
      social_links: [],
      social_statuses: [],
      status: null,
      subscribers_count: null,
      subscriptions_count: null,
      tutorial: {
        android: null,
        ios: null,
        user_id: null,
        web_desktop: null,
        web_mobile: null
      },
      typed_rating: {
        day: null,
        month: null,
        week: null
      }
    },
    loaded: false,
    loading: false,
    error: null
  },
  mutations: {
    /** fetch current user */
    [FETCH_CURRENT_USER + REQUEST](state) {
      state.loading = true;
    },
    [FETCH_CURRENT_USER + SUCCESS](state, user) {
      state.loading = false;
      state.loaded = true;
      state.error = null;
      state.user = user;
    },
    [FETCH_CURRENT_USER + ERROR](state, error) {
      state.loading = false;
      state.loaded = true;
      state.error = error;
      state.user = null;
    },
    [ADD_LOADED + SUCCESS](state) {
      state.loaded = true;
    },
    [CHANGE_FAMILY_STATUS + REQUEST](state) {

    },
    [CHANGE_FAMILY_STATUS + SUCCESS](state, payload) {
      state.user.family_status = payload;
    } ,
    [CHANGE_FAMILY_STATUS + ERROR](state, payload) {

    },
    [ACCEPT_GIFT + REQUEST](state) {

    },
    [ACCEPT_GIFT + SUCCESS](state, payload) {
      if(payload.state){
        this.user.active_gift.id = payload.gift.gift_model.id;
        this.user.active_gift.image = payload.gift.gift_model.image;
        this.user.active_gift.name = payload.gift.gift_model.name;
      }
    },
    [ACCEPT_GIFT + ERROR](state, error) {

    },
    [ADD_POST_TO_AUTH_USER](state) {
      state.user.posts_count = state.user.posts_count + 1;
    },
    [DELETE_POST_FROM_AUTH_USER](state) {
      state.user.posts_count = state.user.posts_count - 1;
    },
    [CHANGE_SUBSCRIBE_USER_COUNT](state, payload) {
      state.user.subscribers_count = state.user.subscriptions_count + payload;
    }
  },
  actions: {
    async [FETCH_CURRENT_USER]({commit}, payload) {
      commit(FETCH_CURRENT_USER + REQUEST);
      const { onSuccess, onFail } = payload;

      try {
        let res = await api.user.getUser();

        //temporary
        localStorage.setItem('user', JSON.stringify(res.data));

        commit(FETCH_CURRENT_USER + SUCCESS, res.data);
        if(onSuccess) onSuccess();
      } catch (e) {
        commit(FETCH_CURRENT_USER + ERROR, e.response);

        //temporary
        if(localStorage.getItem('user')) localStorage.removeItem('user');

        if(onFail) onFail();
      }
    },
    [ADD_LOADED]({commit}) {
      commit(ADD_LOADED + SUCCESS);
    },
    async [CHANGE_FAMILY_STATUS]({commit, state}, payload) {
      commit(CHANGE_FAMILY_STATUS + REQUEST);

      try {
        const data = new FormData();
        data.append('family_status', payload);

        const res = await api.user.changeMySettings(data);
        console.log(res);

        if(res.data.state) {
          commit(CHANGE_FAMILY_STATUS + SUCCESS, payload);
          commit(usersModule + CHANGE_USER_FAMILY_STATUS, {nickname: state.user.nickname, status: payload}, {root: true})
        }
      } catch (e) {
        commit(CHANGE_FAMILY_STATUS + ERROR, e.response)
      }
    },
    async [ACCEPT_GIFT]({commit}, payload) {
      commit(ACCEPT_GIFT + REQUEST);

      const data = new FormData();
      data.append('event_id', payload);

      try {
        let res = await api.user.acceptGift(data);

        commit(ACCEPT_GIFT + SUCCESS, res.data);
        if(res.data.state) commit(giftsModule + ACCEPT_USER_GIFT, payload);
      } catch (e) {
        commit(ACCEPT_GIFT + ERROR, e.response);
      }
    }
  },
  getters: {
    [AUTH_USER](state) {
      return state.user
    },
    [LOADED](state) {
      return state.loaded
    }
  }
};

export default module;
