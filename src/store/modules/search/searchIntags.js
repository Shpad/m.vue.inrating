import api from 'utils/api';

import {SUCCESS} from "Store/types";


/**
 * Types
 * */

export const moduleName = 'searchIntags/';

/** Actions types*/
export const FETCH_INTAGS_POSTS = 'FETCH_INTAGS_POSTS';

/** Mutation types*/


/** Getters types*/
export const INTAGS_POSTS = 'INTAGS_POSTS';

const module = {
    namespaced: true,
    state: {
      allposts: [],
    },
    mutations: {
      [FETCH_INTAGS_POSTS+SUCCESS] (state,data) {
        state.allposts=data;
      }
    },
    actions: {
      async [FETCH_INTAGS_POSTS]({commit}, payload) {
        try {
          let res=await api.search.getIntagsPosts(payload);
          commit(FETCH_INTAGS_POSTS+SUCCESS,res.data.data)
        }
        catch (e) {

        }
      }
    },
    getters: {
      [INTAGS_POSTS] (state) {
        return state.allposts;
      }
    }
  }
;

export default module;
