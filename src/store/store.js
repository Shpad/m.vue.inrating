import Vue from 'vue';
import Vuex from 'vuex';
import countPostOnPage from 'Store/modules/countPostOnPage';
import user from 'Store/modules/authUser/user';
import users from 'Store/modules/users/users';
import gifts from 'Store/modules/authUser/gifts';
import groups from './modules/groups';
import searchIntags from 'Store/modules/search/searchIntags'
import stories from 'Store/modules/stories'
import feeds from 'Store/modules/feeds'
import groupVideos from 'Store/modules/groups/groupVideos';
import groupAlbums from 'Store/modules/groups/groupAlbums';
import groupPosts from 'Store/modules/groups/groupPosts';
import albumPosts from 'Store/modules/albumPosts';
import posts from 'Store/modules/posts';
import usersProfilePosts from 'Store/modules/users/usersProfilePosts';
import mainPosts from 'Store/modules/mainPosts';

import createLogger from 'vuex/dist/logger';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    countPostOnPage,
    user,
    users,
    gifts,
    groups,
    searchIntags,
    stories,
    feeds,
    groupVideos,
    groupAlbums,
    groupPosts,
    albumPosts,
    posts,
    usersProfilePosts,
    mainPosts
  },
  plugins: [createLogger()]
});

export default store;
