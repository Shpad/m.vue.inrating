import axios from 'axios';

export default {
  user: {
    getUser: () =>
      axios.get('/v1/me'),
    subscribeUser: ({data, config = null}) =>
      axios.post('v1/users/subscribe', data, config),
    getUserFeeds: data =>
      axios.post('v1/me/feeds/new/all', data),
    viewFeeds: data =>
      axios.post('v1/me/feeds/view', data),
    unwatchedFeeds: () =>
      axios.get('v1/me/feeds/unwatched?by_type=true'),
    changeMySettings: data =>
      axios.post('v1/me/settings/edit', data),
    sendChinChin: data =>
      axios.post('v1/me/chats/messages/chin', data),
    checkChat: data =>
      axios.post('v1/me/chats/check', data),
    acceptGift: data =>
      axios.post('v1/me/gifts/accept', data)
  },
  users: {
    getUser: data =>
      axios.post('v1/users/get', data),
    subscribeUser: data =>
      axios.post('v1/users/subscribe', data),
    reportPost: data =>
      axios.post('v1/users/posts/report', data),
    getPostStatistics: data =>
      axios.post('v1/users/posts/statistics/get', data),
    profileVisitFromPost: data =>
      axios.post('v1/users/posts/actions/add', data),
    getPost: data =>
      axios.post('v1/users/posts/get', data),
    getPosts: data =>
      axios.post('v1/users/posts/all', data),
    voteUser: data =>
      axios.post('v1/users/vote', data),
    deletePost: data =>
      axios.post('v1/users/posts/delete', data),
    sendViewedPosts: data =>
      axios.post('v1/users/posts/view', data)
  },
  stories: {
    getAllStories: () =>
      axios.get('v1/users/stories/all'),
    deleteStory: data =>
      axios.post('v1/users/posts/delete',data),
  },
  blacklist: {
    addToBlackList: ({data, config = null}) =>
      axios.post('v1/me/settings/blacklist/add', data, config),
  },
  gifts: {
    getMyGifts: data =>
      axios.post('v1/me/gifts/all', data)
  },
  groups: {
    getGroupSubjects:() =>
      axios.get('v1/groups/subjects/all'),
    createGroup: data =>
      axios.post('v1/groups/create', data),
    getGroup: data =>
      axios.post('v1/groups/get', data),
    subscribeToGroup: data =>
      axios.post('v1/groups/join', data),
    getGroupVideos: data =>
      axios.post('v1/groups/videos/all', data),
    getGroupAlbums: data =>
      axios.post('v1/albums/all', data),
    getGroupPosts: data =>
      axios.post('v1/groups/posts/all', data),
    groupRequestApply: data =>
      axios.post('', data)
  },
  albums: {
    getAlbumPosts: data =>
      axios.post('v1/albums/posts/all', data),
    getAlbum: data =>
      axios.post('v1/albums/get', data)
  },
  search: {
    getIntagsPosts: data =>
      axios.post('v1/users/posts/search',data),
  }
}
