import api from 'utils/api';
import router from '../../router'
import {ERROR, REQUEST, SUCCESS} from "Store/types";


/**
 * Types
 * */

export const moduleName = 'groups/';

/** Actions types*/
export const CREATE_GROUP = 'CREATE_GROUP';SUBSCRIBE_USER
export const FETCH_GROUP_SUBJECTS = 'FETCH_GROUP_SUBJECTS';
export const FETCH_CURRENT_GROUP = 'FETCH_CURRENT_GROUP';
export const SUBSCRIBE_USER = 'SUBSCRIBE_USER';

/** Mutation types*/
// Actions types + REQUEST or SUCCESS or ERROR
export const FETCH_GROUP_SUBJECTS_SUCCESS = 'FETCH_GROUP_SUBJECTS';


/** Getters types*/
export const CURRENT_GROUP = 'CURRENT_GROUP';
export const GROUP_SUBJECTS = 'GROUP_SUBJECTS';


const module = {
  namespaced: true,
  state: {
    groups: {},
    lastPage: 1,
    nextPage: 2,
    groupAlbums: {},
    loaded: false,
    loading: false,
    groupSubjects: {},
  },
  mutations: {
    [FETCH_CURRENT_GROUP + REQUEST](state) {
      state.loading = true;
    },
    [FETCH_CURRENT_GROUP + SUCCESS](state, group) {
      state.loading = false;
      state.loaded = true;
      state.groups = {
        ...state.groups,
        [group.slug]: group,
      }
    },
    [FETCH_CURRENT_GROUP + ERROR](state) {
      state.loading = false;
      state.loaded = true;
    },
    [FETCH_GROUP_SUBJECTS_SUCCESS](state, groupSub) {
      state.groupSubjects = groupSub;
    },
    [SUBSCRIBE_USER + SUCCESS](state, data) {
      state.groups[data.slug].group_user = data.info;
    },
  },
  actions: {
    async [CREATE_GROUP]({commit}, payload) {
      commit(FETCH_CURRENT_GROUP + REQUEST);

      try {
        let res = await api.groups.createGroup(payload);
        commit(FETCH_CURRENT_GROUP + SUCCESS, res.data);
        router.push('/group/' + res.data.slug);
      } catch (e) {

      }
    },
    async [FETCH_GROUP_SUBJECTS]({commit}) {
      try {
        let res = await api.groups.getGroupSubjects();

        commit(FETCH_GROUP_SUBJECTS_SUCCESS, res.data);
      } catch (e) {
      }
    },
    async [FETCH_CURRENT_GROUP]({commit}, payload) {
      commit(FETCH_CURRENT_GROUP + REQUEST);
      const data = new FormData();
      data.append('slug', payload);

      try {
        let res = await api.groups.getGroup(data);
        commit(FETCH_CURRENT_GROUP + SUCCESS, res.data);
      }
      catch (e) {
      }
    },
    async [SUBSCRIBE_USER]({commit}, payload) {
      const {  onFail } = payload;
      try {
        let res = await api.groups.subscribeToGroup(payload.data);
        let data = {
          info: res.data,
          slug: payload.get('group_slug'),
        };

        commit(SUBSCRIBE_USER + SUCCESS, data);
      }
      catch(e) {
          onFail();
      }
    }
  },
  getters: {
    [GROUP_SUBJECTS](state) {
      return state.groupSubjects;
    },
    [CURRENT_GROUP]: state => slug => {
      return state.groups[slug];
    }
  }
};

export default module;
